package configuration;

public class Configuration {

    public static final String PROD_URL = "https://www.n11.com/";
    public static final int WAIT_SHORT = 15;
    public static final int WAIT_MEDIUM = 12;
    public static final int WAIT_LONG = 18;

}
