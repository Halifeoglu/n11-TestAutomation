
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import java.util.Objects;



public class N11Page extends BasePage{

    public N11Page(WebDriver driver) {
        super(driver);
    }

    private By loginbutton= By.cssSelector(".btnSignIn");
    private By email=By.id("email");
    private By password=By.id("password");
    private By signIn=By.id("loginButton");
    private By searchDataField=By.id("searchData");
    private By searchButon=By.cssSelector(".searchBtn");
    private By secondPageClick=By.cssSelector(".pagination>a:nth-child(2)");
    private By addFavoriteButton=By.cssSelector(".resultListGroup>div.listView >ul>li:nth-child(3)>div>div.proDetail>span");
    private By thirthProduct=By.cssSelector(".resultListGroup>div.listView >ul>li:nth-child(3)>div>div.pro>a");
    private By inFavoritePageProduct=By.cssSelector(".wishListGroup>div.listView >ul>li>div>div.pro>a");
    private By goToMyFavoritePage=By.xpath("//*[@href='https://www.n11.com/hesabim/istek-listelerim']");
    private By myFavoriteClick=By.xpath("//*[@href='https://www.n11.com/hesabim/favorilerim']");
    private By deleteFavorite=By.cssSelector(".wishListGroup>div.listView >ul>li>div>div.wishProBtns>span");
    private By acceptMessage=By.cssSelector(".btnHolder");
    private By messageTitle=By.cssSelector(".lightBox>div>span.message");

    public static String thirthProductText;
    public static String inFavoritePageProductText;



    public boolean secondPageControl(String secondPageInUrl){
        return driver.getCurrentUrl().contains(secondPageInUrl);
    }

    public boolean homePageControl(String homePageUrl){
        return driver.getCurrentUrl().contains(homePageUrl);
    }

    public void login(String mail, String pass){
        clickTo(loginbutton);
        clickandType(email , mail);
        clickandType(password, pass);
        clickTo(signIn);
    }

    public void searchProducts(String productName){
        clickandType(searchDataField, productName);
        clickTo(searchButon);
    }

    public boolean userControl(String userInformation) {
        String getUserInfo=driver.findElement(By.cssSelector(".myAccount>a.menuLink.user")).getText();
        return Objects.equals(getUserInfo, userInformation);
    }

    public boolean wantedProductControl(String productName){
        String getSearchDataText=driver.findElement(By.cssSelector(".resultText >h1")).getText();
        return Objects.equals(getSearchDataText, productName);
    }

    public void open2ndPage() {
        scrollTo(driver.findElement(secondPageClick));
        clickTo(secondPageClick);
    }

    public void thirthProductAddFavorite() {
        scrollTo(driver.findElement(addFavoriteButton));
        thirthProductText=driver.findElement(thirthProduct).getAttribute("title");
        clickTo(addFavoriteButton);
        favoriteControl();
    }

    public void favoriteControl(){
        WebElement element = driver.findElement(By.cssSelector(".myAccount"));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).build().perform();
        clickTo(goToMyFavoritePage);
        clickTo(myFavoriteClick);
        inFavoritePageProductText=driver.findElement(inFavoritePageProduct).getAttribute("title");
    }

    public boolean productMatch() {
        return Objects.equals(thirthProductText, inFavoritePageProductText);
    }

    public void deleteProductInFavoritePage() {
        clickTo(deleteFavorite);
    }


    public boolean messageControl(String message) {
        waitForVisibility(By.cssSelector(".lightBox"));
        String getMessage=driver.findElement(messageTitle).getText();
        return Objects.equals(getMessage, message);
    }

    public void complete() {
        clickTo(acceptMessage);
    }

    public boolean emptyFavoritePageControl() {
        return isElementDisplayed(By.cssSelector(".emptyWatchList"));
    }


}
