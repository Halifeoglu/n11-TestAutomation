import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.assertThat;


public class N11PageTest extends BaseTest{

    @Test
    public void N11Test() {
        N11Page n11Page=new N11Page(driver);

        //http://www.n11.com<http://www.n11.com/> sitesine gelecek ve anasayfanin acildigini onaylayacak
        assertThat("When user opened the n11 site , must see correct page", n11Page.homePageControl("https://www.n11.com/"));

        UserPool.userInformation().standartLogin(n11Page);
        //Login ekranini acip, bir kullanici ile login olacak
        assertThat("When user was logIn  , must see own name", n11Page.userControl("Samet Halifeoglu"));

        n11Page.searchProducts("Samsung");
        //Ekranin ustundeki Search alanina 'samsung' yazip Ara butonuna tiklayacak
        //Gelen sayfada samsung icin sonuc bulundugunu onaylayacak
        assertThat("When user searched Samsung's products  , must see Samsung results", n11Page.wantedProductControl("Samsung"));

        n11Page.open2ndPage();
        //Arama sonuclarindan 2. sayfaya tiklayacak ve acilan sayfada 2. sayfanin su an gosterimde oldugunu onaylayacak
        assertThat("When user clicked 2nd Page  , must see correct results", n11Page.secondPageControl("pg=2"));

        n11Page.thirthProductAddFavorite();
        //Ustten 3. urunun icindeki 'favorilere ekle' butonuna tiklayacak
        //Ekranin en ustundeki 'favorilerim' linkine tiklayacak
        //Acilan sayfada bir onceki sayfada izlemeye alinmis urunun bulundugunu onaylayacak
        assertThat("When user opened favorite Page  , must see equals product", n11Page.productMatch());

        n11Page.deleteProductInFavoritePage();
        //Favorilere alinan bu urunun yanindaki 'Kaldir' butonuna basarak, favorilerimden cikaracak
        assertThat("When the user deletes the product from the favorites, should see the message.", n11Page.messageControl("Ürününüz listeden silindi."));

        n11Page.complete();
        //Sayfada bu urunun artik favorilere alinmadigini onaylayacak.
        assertThat("When user deleted product from favorite Page , must see empty favorite page ", n11Page.emptyFavoritePageControl());

    }
}
