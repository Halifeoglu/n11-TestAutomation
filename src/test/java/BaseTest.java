import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static configuration.Configuration.PROD_URL;

public class BaseTest {

    WebDriver driver;

    @BeforeMethod
    public void startUp(){
        WebDriverManager.chromedriver().setup();

        ChromeOptions chromeOptions=new ChromeOptions();
        chromeOptions.addArguments("window-size=1440,3000");
        //chromeOptions.addArguments("headless");

        driver=new ChromeDriver(chromeOptions);
        driver.navigate().to(PROD_URL);
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);

    }


    @AfterMethod
    public void FailedTestCaseScreenShot(ITestResult result) throws IOException {

        if (ITestResult.FAILURE == result.getStatus()) {

            TakesScreenshot ts = (TakesScreenshot) driver;
            File source = ts.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(source, new File("./FailureScreenShot/" + result.getName() + ".png"));
            driver.close();
        } else {
            driver.close();
        }
    }

}
